var new_goal = argument[0];

from_intersection = to_intersection;
to_intersection = new_goal;

var to_intersection_ref = global.game_map[to_intersection];

var to_x = ds_map_find_value(to_intersection_ref, global.intersection_location_x_key);
var to_y = ds_map_find_value(to_intersection_ref, global.intersection_location_y_key);

if (x == to_x) {
	if (y < to_y) {
		ghost_direction = direction_down;
	} else {
		ghost_direction = direction_up;
	}
} else if (y == to_y) {
	if ((from_intersection == 33) && (to_intersection == 36)) {
		ghost_direction = direction_left;
	} else if ((from_intersection == 36) && (to_intersection == 33)) {
		ghost_direction = direction_right;
	} else {
		if (x < to_x) {
			ghost_direction = direction_right;
		} else {
			ghost_direction = direction_left;
		}
	}
}