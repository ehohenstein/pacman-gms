var current_intersection = global.game_map[to_intersection];

var options = ds_map_find_value(current_intersection, global.intersection_options_key);

var available_count = 0;
for (var i = 0; i < array_length_1d(options); i++) {
	if (can_choose_option(options[i])) {
		var option_weight = ds_map_find_value(options[i], global.option_weight_key);
		var option_destination = ds_map_find_value(options[i], global.option_destination_key);
		var option_count = 0;
		if (option_destination != from_intersection) {
			option_count = (option_weight * 3);
		} else {
			option_count = option_weight;
		}
		available_count += option_count;
	}
}

var choice_index = irandom(available_count - 1);

available_count = 0;
for (var i = 0; i < array_length_1d(options); i++) {
	if (can_choose_option(options[i])) {
		var option_weight = ds_map_find_value(options[i], global.option_weight_key);
		var option_destination = ds_map_find_value(options[i], global.option_destination_key);
		var option_count = 0;
		if (option_destination != from_intersection) {
			option_count = (option_weight * 3);
		} else {
			option_count = option_weight;
		}
		if ((choice_index >= available_count) && (choice_index < (available_count + option_count))) {
			return option_destination;
		}
		available_count += option_count;
	}
}