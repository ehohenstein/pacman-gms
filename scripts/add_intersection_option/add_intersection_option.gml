var intersection = argument[0];
var index = argument[1];
var option = argument[2];

var options = ds_map_find_value(intersection, global.intersection_options_key);

options[index] = option;

ds_map_set(intersection, global.intersection_options_key, options);
