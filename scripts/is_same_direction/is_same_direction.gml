var from = argument[0];
var destination = argument[1];
var check_direction = argument[2];

var destination_intersection = global.game_map[destination];

var destination_x = ds_map_find_value(destination_intersection, global.intersection_location_x_key);
var destination_y = ds_map_find_value(destination_intersection, global.intersection_location_y_key);

if (x == destination_x) {
	if (destination_y > y) {
		return (check_direction == direction_down);
	} else {
		return (check_direction == direction_up);
	}
} else if (y == destination_y) {
	if ((from == 33) && (destination == 36)) {
		return (check_direction == direction_left);
	} else if ((from == 36) && (destination == 33)) {
		return (check_direction == direction_right);
	} else {
		if (destination_x > x) {
			return (check_direction == direction_right);
		} else {
			return (check_direction == direction_left);
		}
	}
}
return false;