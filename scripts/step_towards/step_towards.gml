var from_intersection_ref = global.game_map[from_intersection];
var to_intersection_ref = global.game_map[to_intersection];

var from_x = ds_map_find_value(from_intersection_ref, global.intersection_location_x_key);
var from_y = ds_map_find_value(from_intersection_ref, global.intersection_location_y_key);

var to_x = ds_map_find_value(to_intersection_ref, global.intersection_location_x_key);
var to_y = ds_map_find_value(to_intersection_ref, global.intersection_location_y_key);

var delta = 0;

if (from_x == to_x) {
	if (to_y > from_y) {
		delta = ghost_speed;
	} else {
		delta = ghost_speed * -1;
	}
	
	y += (delta * speed_scale);

	if (abs(y - from_y) >= abs(to_y - from_y)) {
		y = to_y;
		return true;
	}
	return false;
} else if (from_y == to_y) {
	if ((from_intersection == 33) && (to_intersection == 36)) {
		x -= ghost_speed;
		if (x < 26) {
			x = 617;
		}
		if ((x <= to_x) && (x > from_x)) {
			x = to_x;
			return true;
		}
		return false;
	} else if ((from_intersection == 36) && (to_intersection == 33)) {
		x += ghost_speed;
		if (x > 617) {
			x = 26;
		}
		if ((x >= to_x) && (x < from_x)) {
			x = to_x;
			return true;
		}
		return false;
	} else {
		if (to_x > from_x) {
			delta = ghost_speed;
		} else {
			delta = ghost_speed * -1;
		}

		x += (delta * speed_scale);

		if (abs(x - from_x) >= abs(to_x - from_x)) {
			x = to_x;
			return true;
		}
		return false;
	}
}


