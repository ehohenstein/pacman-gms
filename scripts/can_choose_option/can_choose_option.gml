var option = argument[0];

var allowed_in_jail = ds_map_find_value(option, global.option_available_to_jailed_ghost_key);
var allowed_free = ds_map_find_value(option, global.option_available_to_free_ghost_key);
var allowed_eaten = ds_map_find_value(option, global.option_available_to_eaten_ghost_key);

if (in_jail) {
	return allowed_in_jail;
} else if (eaten) {
	return allowed_eaten;
} else {
	return allowed_free;
}
return false;