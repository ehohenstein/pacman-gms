var location_x = argument[0];
var location_y = argument[1];
var option_count = (argument_count - 2) / 2;

var intersection = create_intersection(option_count, location_x, location_y);

for (var i = 0; i < option_count; i++) {
	var destination = argument[(i * 2) + 2];
	var available_to_eaten_ghost = argument[(i * 2) + 3];
	add_intersection_option(intersection, i, create_intersection_option(destination, available_to_eaten_ghost));
}

return intersection;