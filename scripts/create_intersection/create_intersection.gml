var option_count = argument[0];
var location_x = argument[1];
var location_y = argument[2];

var intersection = ds_map_create();

var options_array = array_create(option_count, noone);

ds_map_set(intersection, global.intersection_options_key, options_array);
ds_map_set(intersection, global.intersection_location_x_key, location_x);
ds_map_set(intersection, global.intersection_location_y_key, location_y);

return intersection;