var current_intersection = global.game_map[to_intersection];

var options = ds_map_find_value(current_intersection, global.intersection_options_key);

var in_same_direction = noone;
var in_wanted_direction = noone;

for (var i = 0; i < array_length_1d(options); i++) {
	if (can_choose_option(options[i])) {
		var option_destination = ds_map_find_value(options[i], global.option_destination_key);
		if (is_same_direction(to_intersection, option_destination, ghost_direction)) {
			in_same_direction = option_destination;
		} else if (is_same_direction(to_intersection, option_destination, want_direction)) {
			in_wanted_direction = option_destination;
		}
	}
}

if (in_wanted_direction != noone) {
	from_intersection = to_intersection;
	to_intersection = in_wanted_direction;
	ghost_direction = want_direction;
} else if (in_same_direction != noone) {
	from_intersection = to_intersection;
	to_intersection = in_same_direction;
} else {
	stopped = true;
}