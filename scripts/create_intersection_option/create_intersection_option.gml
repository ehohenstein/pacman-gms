var destination = argument[0];
var available_to_eaten = argument[1];
var weight = 1;
if (argument_count > 2) {
	weight = argument[2];
}
var available_to_jailed = false;
if (argument_count > 3) {
	available_to_jailed = argument[3];
}
var available_to_free = true;
if (argument_count > 4) {
	available_to_free = argument[4];
}

var option = ds_map_create();

ds_map_set(option, global.option_destination_key, destination);
ds_map_set(option, global.option_available_to_eaten_ghost_key, available_to_eaten);
ds_map_set(option, global.option_weight_key, weight);
ds_map_set(option, global.option_available_to_jailed_ghost_key, available_to_jailed);
ds_map_set(option, global.option_available_to_free_ghost_key, available_to_free);

return option;