{
    "id": "3c1a1090-7c3e-4c1d-beef-b51114ee17f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pinky",
    "eventList": [
        {
            "id": "622d5724-788f-4e7e-9e2f-9cbb1a9cae29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c1a1090-7c3e-4c1d-beef-b51114ee17f8"
        }
    ],
    "maskSpriteId": "0839a526-381b-49b2-9fe9-8692e5df78ce",
    "overriddenProperties": null,
    "parentObjectId": "3a283dc7-3dfe-4290-9d8c-e538402fd79e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
    "visible": true
}