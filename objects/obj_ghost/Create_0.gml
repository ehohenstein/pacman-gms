event_inherited();

in_jail = true;

ghost_speed = 1.5;

ghost_direction = direction_up;

init_image_index = 0;

power_timer = 0;
eaten_index = 0;

speed_scale = power(1.05, global.level);
