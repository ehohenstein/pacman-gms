{
    "id": "3a283dc7-3dfe-4290-9d8c-e538402fd79e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ghost",
    "eventList": [
        {
            "id": "43aaa25b-c48e-4faa-be0d-ccc41c88303e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a283dc7-3dfe-4290-9d8c-e538402fd79e"
        },
        {
            "id": "40d5b85e-79f7-4a76-90ec-9db8774a9c13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3a283dc7-3dfe-4290-9d8c-e538402fd79e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fc45faa0-1298-469d-bdb6-d809f69f732a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}