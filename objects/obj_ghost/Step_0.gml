if (global.game_state == global.game_state_init) {
	image_index = init_image_index;
} else {
	if (eaten) {
		if (eaten_timer > 0) {
			sprite_index = spr_bonus_points;
			animation_frame = 0;
			animation_frames = 1;
			animation_offset = eaten_index;
			animate_direction = false;
			eaten_timer--;
		} else {
			sprite_index = spr_power_ghost;
			animation_frames = 1;
			animation_offset = 4;
			animate_direction = true;
			ghost_speed = 2.5;
		}
	} else if (global.game_state == global.game_state_normal) {
		if (power_timer > 0) {
			ghost_speed = 1.4;
			animate_direction = false;
			sprite_index = spr_power_ghost;
			if (power_timer > (room_speed * 3)) {
				animation_frames = 2;
			} else {
				animation_frames = 4;
			}
			animation_offset = 0;
			power_timer--;
		} else {
			sprite_index = normal_sprite;
			animation_frames = 2;
			animation_offset = 0;
			animate_direction = true;
			ghost_speed = 1.7;
		}
	}

	event_inherited();

	if ((global.game_state != global.game_state_reset) && (global.game_state != global.game_state_paused) && (global.game_state != global.game_state_level_complete)) {
		if (step_towards()) {
			if (eaten && (to_intersection == 28)) {
				eaten = false;
				power_timer = 0;
				to_intersection = 24;
				from_intersection = 28;
			} else {
				select_new_target(choose_ghost_goal());
			}
		}
	}
}