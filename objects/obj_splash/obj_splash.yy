{
    "id": "83232c29-b9f3-4bcd-a7ea-512054b6b8de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_splash",
    "eventList": [
        {
            "id": "de9b0ebe-62d9-45b4-b93b-b637369278c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83232c29-b9f3-4bcd-a7ea-512054b6b8de"
        },
        {
            "id": "a9b5a778-81a3-4338-afef-bf07d10f6a44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "83232c29-b9f3-4bcd-a7ea-512054b6b8de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "938191f4-fc99-4c44-aa20-0e6a4ec5c030",
    "visible": true
}