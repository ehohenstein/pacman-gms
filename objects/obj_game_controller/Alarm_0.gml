if (lives > 0) {
	global.game_state = global.game_state_init;

	second_stage = true;

	var blinky = instance_find(obj_blinky, 0);
	blinky.visible = true;
	blinky.ghost_direction = blinky.direction_left;
	blinky.x = 321;
	blinky.y = 305;
	blinky.from_intersection = 24;
	blinky.to_intersection = 23;
	blinky.eaten = false;

	var pinky = instance_find(obj_pinky, 0);
	pinky.visible = true;
	pinky.ghost_direction = pinky.direction_up;
	pinky.x = 321;
	pinky.y = 364;
	pinky.from_intersection = 31;
	pinky.to_intersection = 28;
	pinky.eaten = false;

	var inky = instance_find(obj_inky, 0);
	inky.visible = true;
	inky.ghost_direction = inky.direction_down;
	inky.x = 281;
	inky.y = 364;
	inky.from_intersection = 27;
	inky.to_intersection = 30;
	inky.in_jail = true;
	inky.alarm[0] = 5 * room_speed;
	inky.eaten = false;

	var clyde = instance_find(obj_clyde, 0);
	clyde.visible = true;
	clyde.ghost_direction = clyde.direction_down;
	clyde.x = 361;
	clyde.y = 364;
	clyde.from_intersection = 29;
	clyde.to_intersection = 32;
	clyde.in_jail = true;
	clyde.alarm[0] = 10 * room_speed;
	clyde.eaten = false;

	var pacman = instance_find(obj_pacman, 0);
	pacman.visible = true;
	pacman.ghost_direction = pacman.direction_left;
	pacman.want_direction = pacman.direction_left;
	pacman.x = 321;
	pacman.y = 541;
	pacman.from_intersection = 52;
	pacman.to_intersection = 51;
	pacman.eating = 0;
} else {
	global.game_state = global.game_state_over;
}

alarm[1] = 2.1 * room_speed;
