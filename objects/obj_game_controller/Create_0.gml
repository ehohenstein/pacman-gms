event_inherited();

global.game_state = global.game_state_init;

second_stage = false;

level_complete_timer = 0;
was_complete = false;

alarm[0] = 2.1 * room_speed;

for (var i = 0; i < instance_number(obj_animated); i++) {
	var instance = instance_find(obj_animated, i);
	instance.visible = false;
}

audio_play_sound(snd_game_start, 1, false);
