event_inherited();

var power_count = 0;
var eaten_count = 0;
for (var i = 0; i < instance_number(obj_ghost); i++) {
	var instance = instance_find(obj_ghost, i);
	if (instance.eaten) {
		eaten_count++;
	}
	if (instance.power_timer > 0) {
		power_count++;
	}
}

if (global.game_state == global.game_state_normal) {
	if (!audio_is_playing(snd_background)) {
		audio_play_sound(snd_background, 0, true);
	}
	
	if (power_count > 0) {
		if (eaten_count > 0) {
			if (audio_is_playing(snd_power)) {
				audio_stop_sound(snd_power);
			}
			if (!audio_is_playing(snd_ghost_eaten)) {
				audio_play_sound(snd_ghost_eaten, 2, true);
			}
		} else {
			if (audio_is_playing(snd_ghost_eaten)) {
				audio_stop_sound(snd_ghost_eaten);
			}
			if (!audio_is_playing(snd_power)) {
				audio_play_sound(snd_power, 2, true);
			}
		}
	} else {
		if (audio_is_playing(snd_power)) {
			audio_stop_sound(snd_power);
		}
		if (audio_is_playing(snd_ghost_eaten)) {
			audio_stop_sound(snd_ghost_eaten);
		}
	}
} else {
	if (audio_is_playing(snd_background)) {
		audio_stop_sound(snd_background);
	}
}

one_up_timer--;
if (one_up_timer == 0) {
	one_up_timer = one_up_timer_frames;
	draw_one_up = !draw_one_up;
}

if (global.game_state == global.game_state_level_complete) {
	if (!was_complete) {
		audio_stop_all();
		alarm[2] = room_speed;
		was_complete = true;
	}

	if (level_complete_timer > 0) {
		level_complete_timer--;
		if (level_complete_timer == 0) {
			room_goto(room_intermission);
		}
		var target_bg = spr_game_bg;
		if ((floor(level_complete_timer / 10) mod 2) == 1) {
			target_bg = spr_game_bg_white;
		}
		var bg_layer_id = layer_get_id("Background");
		var bg_id = layer_background_get_id(bg_layer_id);
		if (layer_background_get_sprite(bg_id) != target_bg) {
			layer_background_sprite(bg_id, target_bg);
		}
	}
}
