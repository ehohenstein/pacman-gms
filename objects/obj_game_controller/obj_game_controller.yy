{
    "id": "1ed946ba-4a36-4fa0-ae25-3115f09ed45b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_controller",
    "eventList": [
        {
            "id": "8e7bdd5a-9ad3-4e8a-bb5b-ddd5e9ec84a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ed946ba-4a36-4fa0-ae25-3115f09ed45b"
        },
        {
            "id": "b7483f82-bcc5-4044-9099-19e249ed4ef3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1ed946ba-4a36-4fa0-ae25-3115f09ed45b"
        },
        {
            "id": "c52e5245-3935-4764-ae11-7a6da6cf1ae7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1ed946ba-4a36-4fa0-ae25-3115f09ed45b"
        },
        {
            "id": "cf1c3004-cc0b-43d8-ba13-14f1350abd52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "1ed946ba-4a36-4fa0-ae25-3115f09ed45b"
        },
        {
            "id": "b83ecbd8-8b17-4f4e-9613-7953ed050400",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1ed946ba-4a36-4fa0-ae25-3115f09ed45b"
        },
        {
            "id": "0296af76-122f-4293-ade8-9695f58f3933",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "1ed946ba-4a36-4fa0-ae25-3115f09ed45b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6331fc9c-388b-4388-897b-99fe52867775",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}