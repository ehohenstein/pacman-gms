event_inherited();

if (global.game_state == global.game_state_init) {
	if (!second_stage) {
		render_text = "PLAYER ONE";
		render_text_x = 231;
		render_text_y = 297;
		render_text_color = c_aqua;
		event_user(0);
	}
	render_text = "READY!";
	render_text_x = 273;
	render_text_y = 415;
	render_text_color = c_yellow;
	event_user(0);
}

if (global.game_state == global.game_state_over) {
	render_text_color = c_red;
	render_text = "GAME  OVER";
	render_text_x = 231;
	render_text_y = 415;
	event_user(0);
}

render_text_color = c_white;

for (var i = 1; i < lives; i++) {
	draw_sprite(spr_pacman, 12, 104 + (i * 39), 710);
}