direction_up = 0;
direction_down = 1;
direction_left = 2;
direction_right = 3;

animation_frame_count = 10;
animation_timer = animation_frame_count;

animate_direction = true;
animation_frame = 0;
animation_frames = 2;
animation_offset = 0;

speed_scale = 1.0;

stopped = false;
eaten = false;
