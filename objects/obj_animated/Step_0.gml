var frame_offset = animation_offset;
if (animate_direction) {
	frame_offset += (ghost_direction * animation_frames);
}
image_index = frame_offset + animation_frame;

animation_timer--;
if ((animation_timer == 0) && (!stopped || (animation_frame != 0))) {
	animation_timer = animation_frame_count;
	animation_frame = (animation_frame + 1) mod animation_frames;
}