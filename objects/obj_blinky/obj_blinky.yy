{
    "id": "d59c5a40-9d04-45a9-8a44-3ab2e70d0801",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blinky",
    "eventList": [
        {
            "id": "2eff29d5-7555-43fe-9294-0bd07b606486",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d59c5a40-9d04-45a9-8a44-3ab2e70d0801"
        }
    ],
    "maskSpriteId": "0839a526-381b-49b2-9fe9-8692e5df78ce",
    "overriddenProperties": null,
    "parentObjectId": "3a283dc7-3dfe-4290-9d8c-e538402fd79e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
    "visible": true
}