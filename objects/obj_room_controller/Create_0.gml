render_text_x = 0;
render_text_y = 1;
render_text_color = c_white;
render_text = "";

c_pink = make_color_rgb(255, 192, 255);
c_ltorange = make_color_rgb(255, 192, 176);

one_up_timer_frames = 15;
one_up_timer = one_up_timer_frames;
draw_one_up = true;

audio_stop_all();
