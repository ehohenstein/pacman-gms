var text_length = string_byte_length(render_text);

for (var i = 0; i < text_length; i++) {
	var c = string_byte_at(render_text, i + 1);

	var spr_index = 0;
	if ((c >= 48) && (c <= 57)) {
		spr_index = (c - 48);
	} else if ((c >= 65) && (c <= 90)) {
		spr_index = ((c - 65) + 10);
	} else if (c == 32) {
		spr_index = 36;
	} else if (c == 33) {
		spr_index = 37
	} else if (c == 45) {
		spr_index = 40;
	} else if (c == 46) {
		spr_index = 41;
	} else if (c == 64) {
		spr_index = 44;
	} else if (c == 47) {
		spr_index = 45;
	} else if (c == 34) {
		spr_index = 46;
	}
	draw_sprite_ext(spr_font, spr_index, render_text_x, render_text_y, 1, 1, 0, render_text_color, 1);
	render_text_x += 18;
}
