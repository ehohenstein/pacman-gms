render_text_color = c_white;

if (draw_one_up) {
	render_text = "1UP";
	render_text_x = 107;
	render_text_y = 20;
	event_user(0);
}

render_text = string(score);
render_text_x = (161 - (18 * string_byte_length(render_text)));
render_text_y = 39;
event_user(0);

render_text = "HIGH SCORE";
render_text_x = 231;
render_text_y = 20;
event_user(0);

render_text = string(global.high_score);
render_text_x = (375 - (18 * string_byte_length(render_text)));
render_text_y = 39;
event_user(0);

render_text = "2UP";
render_text_x = 479;
render_text_y = 20;
event_user(0);

render_text = "0";
render_text_x = 515;
render_text_y = 39;
event_user(0);

