{
    "id": "dc36b19c-494f-4002-9222-3a140e6ba5b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clyde",
    "eventList": [
        {
            "id": "352760a9-cea3-483e-88a3-dd3017ae3ca6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc36b19c-494f-4002-9222-3a140e6ba5b6"
        },
        {
            "id": "cf3d83ca-b074-408f-b851-37d3f78017ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dc36b19c-494f-4002-9222-3a140e6ba5b6"
        }
    ],
    "maskSpriteId": "0839a526-381b-49b2-9fe9-8692e5df78ce",
    "overriddenProperties": null,
    "parentObjectId": "3a283dc7-3dfe-4290-9d8c-e538402fd79e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
    "visible": true
}