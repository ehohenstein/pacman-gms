animation_frame++;

if (animating_left) {
	pacman_pos -= pacman_left_speed;
	blinky_pos -= blinky_left_speed;
	
	if (blinky_pos < -30) {
		animating_left = false;
		pacman_pos -= 270;
	}
} else {
	pacman_pos += pacman_right_speed;
	blinky_pos += blinky_right_speed;

	if (pacman_pos > 750) {
		room_goto(room_game);
	}
}
