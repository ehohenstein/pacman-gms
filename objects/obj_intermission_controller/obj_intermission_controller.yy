{
    "id": "8cb4e61a-40bd-4a1b-9b49-eae50af8a58c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_intermission_controller",
    "eventList": [
        {
            "id": "303e87fa-7b48-4251-99fe-94800908de59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8cb4e61a-40bd-4a1b-9b49-eae50af8a58c"
        },
        {
            "id": "23f3ffcf-21e5-40e3-9602-10ab1798e879",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8cb4e61a-40bd-4a1b-9b49-eae50af8a58c"
        },
        {
            "id": "35033917-d4ac-491c-bcf4-c63605ad76d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8cb4e61a-40bd-4a1b-9b49-eae50af8a58c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6331fc9c-388b-4388-897b-99fe52867775",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}