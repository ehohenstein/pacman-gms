var pacman_sprite_frame = (floor(animation_frame / 10) mod 4);
var blinky_sprite_frame = pacman_sprite_frame mod 2;

if (animating_left) {
	draw_sprite(spr_pacman, 8 + pacman_sprite_frame, pacman_pos, 361);
	draw_sprite(spr_blinky, 4 + blinky_sprite_frame, blinky_pos, 361);
} else {
	draw_sprite_ext(spr_pacman, 12 + pacman_sprite_frame, pacman_pos, 344, 2.5, 2.5, 0, c_white, 1);
	draw_sprite(spr_power_ghost, blinky_sprite_frame, blinky_pos, 361);
}
