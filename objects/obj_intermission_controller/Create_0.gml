event_inherited();

global.level++;

audio_play_sound(snd_intermission, 1, false);

animation_frame = 0;
animating_left = true;

pacman_pos = 650;
blinky_pos = 720;

pacman_left_speed = 2.3;
blinky_left_speed = 2.4;

pacman_right_speed = 3.3;
blinky_right_speed = 2.4;
