if (keyboard_check_pressed(vk_anykey)) {
	room_goto_next();
}

animation_frame++;

if ((animation_frame >= show_animation) && (animation_frame < show_power_animation)) {
	pacman_pos -= pacman_left_speed;
	blinky_pos -= ghost_left_speed;
	pinky_pos -= ghost_left_speed;
	inky_pos -= ghost_left_speed;
	clyde_pos -= ghost_left_speed;
}

if ((animation_frame >= show_power_animation) && (animation_frame < show_pacman_right)) {
	pacman_pos -= pacman_left_speed;
}

if (animation_frame >= show_pacman_right) {
	if (pause_frames == 0) {
		pacman_pos += pacman_right_speed;
		if (pacman_pos > 700) {
			room_restart();
		}
	}
}

if (animation_frame >= show_power_animation) {
	if (pause_frames > 0) {
		pause_frames--;
	} else {
		if ((blinky_pos - pacman_pos) < eat_distance) {
			if (blinky_eaten == 0) {
				blinky_eaten = animation_frame;
				pause_frames = pause_frame_count;
			}
		} else {
			blinky_pos += ghost_right_speed;
		}

		if ((pinky_pos - pacman_pos) < eat_distance) {
			if (pinky_eaten == 0) {
				pinky_eaten = animation_frame;
				pause_frames = pause_frame_count;
			}
		} else {
			pinky_pos += ghost_right_speed;
		}

		if ((inky_pos - pacman_pos) < eat_distance) {
			if (inky_eaten == 0) {
				inky_eaten = animation_frame;
				pause_frames = pause_frame_count;
			}
		} else {
			inky_pos += ghost_right_speed;
		}

		if ((clyde_pos - pacman_pos) < eat_distance) {
			if (clyde_eaten == 0) {
				clyde_eaten = animation_frame;
				pause_frames = pause_frame_count;
			}
		} else {
			clyde_pos += ghost_right_speed;
		}
	}
}
