event_inherited();

lives = 3;
score = 0;
global.level = 0;

animation_frame = 0;

step_frames = 40;
pause_frames = 0;
pause_frame_count = 30;

eat_distance = 8;
ghost_eaten = 0;

show_blinky_sprite = step_frames;
show_blinky_name = step_frames * 2;
show_blinky_nickname = step_frames * 3;
blinky_eaten = 0;

show_pinky_sprite = step_frames * 4;
show_pinky_name = step_frames * 5;
show_pinky_nickname = step_frames * 6;
pinky_eaten = 0;

show_inky_sprite = step_frames * 7;
show_inky_name = step_frames * 8;
show_inky_nickname = step_frames * 9;
inky_eaten = 0;

show_clyde_sprite = step_frames * 10;
show_clyde_name = step_frames * 11;
show_clyde_nickname = step_frames * 12;
clyde_eaten = 0;

show_points = step_frames * 13;

show_animation = step_frames * 14;
show_power_animation = show_animation + 312;
show_pacman_right = show_power_animation + 5;

pacman_pos = 614;
blinky_pos = 690;
pinky_pos = 724;
inky_pos = 758;
clyde_pos = 792;

pacman_left_speed = 1.5;
ghost_left_speed = 1.62;

pacman_right_speed = 1.8;
ghost_right_speed = 0.9;