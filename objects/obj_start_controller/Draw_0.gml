event_inherited();

render_color = c_white;

render_text = "CREDIT  1";
render_text_x = 87;
render_text_y = 702;
event_user(0);

render_text = "CHARACTER / NICKNAME";
render_text_x = 179;
render_text_y = 96;
event_user(0);

if (animation_frame >= show_blinky_sprite) {
	draw_sprite(spr_blinky, 7, 146, 136);
}

render_text_color = c_red;
if (animation_frame >= show_blinky_name) {
	render_text = "-SHADOW";
	render_text_x = 179;
	render_text_y = 128;
	event_user(0);
}

if (animation_frame >= show_blinky_nickname) {
	render_text = "\"BLINKY\"";
	render_text_x = 377;
	render_text_y = 128;
	event_user(0);
}

if (animation_frame >= show_pinky_sprite) {
	draw_sprite(spr_pinky, 7, 146, 186);
}

render_text_color = c_pink;
if (animation_frame >= show_pinky_name) {
	render_text = "-SPEEDY";
	render_text_x = 179;
	render_text_y = 178;
	event_user(0);
}

if (animation_frame >= show_pinky_nickname) {
	render_text = "\"PINKY\"";
	render_text_x = 377;
	render_text_y = 178;
	event_user(0);
}

if (animation_frame >= show_inky_sprite) {
	draw_sprite(spr_inky, 7, 146, 236);
}

render_text_color = c_aqua;
if (animation_frame >= show_inky_name) {
	render_text = "-BASHFUL";
	render_text_x = 179;
	render_text_y = 228;
	event_user(0);
}

if (animation_frame >= show_inky_nickname) {
	render_text = "\"INKY\"";
	render_text_x = 377;
	render_text_y = 228;
	event_user(0);
}

if (animation_frame >= show_clyde_sprite) {
	draw_sprite(spr_clyde, 7, 146, 286);
}

render_text_color = c_orange;
if (animation_frame >= show_clyde_name) {
	render_text = "-POKEY";
	render_text_x = 179;
	render_text_y = 278;
	event_user(0);
}

if (animation_frame >= show_clyde_nickname) {
	render_text = "\"CLYDE\"";
	render_text_x = 377;
	render_text_y = 278;
	event_user(0);
}

var power_dot_frame = 0;
if (((animation_frame mod 20) >= 10) && (animation_frame >= show_animation)) {
	power_dot_frame = 1;
}

render_text_color = c_white;
if (animation_frame >= show_points) {
	draw_sprite(spr_dot, 0, 231, 461);
	render_text = "10 PTS";
	render_text_x = 274;
	render_text_y = 453;
	event_user(0);

	draw_sprite(spr_power_dot, power_dot_frame, 231, 491);
	render_text = "20 PTS";
	render_text_x = 274;
	render_text_y = 483;
	event_user(0);
}

if (animation_frame >= show_animation) {
	render_text_color = c_pink;
	render_text = "@ 1980 MIDWAY MGF. CO.";
	render_text_x = 127;
	render_text_y = 558;
	event_user(0);

	if (animation_frame < show_power_animation) {
		draw_sprite(spr_power_dot, power_dot_frame, 140, 361);
	}

	var frame = (animation_frame - show_animation);
	var sprite_frame = floor((frame mod 40) / 10);
	var ghost_frame = (sprite_frame mod 2);

	if (pause_frames == 0) {
		var pacman_sprite_frame_offset = 8;
		if (animation_frame >= show_pacman_right) {
			pacman_sprite_frame_offset = 12;
		}
		draw_sprite(spr_pacman, sprite_frame + pacman_sprite_frame_offset, pacman_pos, 361);
	}
		
	if (animation_frame < show_power_animation) {
		draw_sprite(spr_blinky, ghost_frame + 4, blinky_pos, 361);
		draw_sprite(spr_pinky, ghost_frame + 4, pinky_pos, 361);
		draw_sprite(spr_inky, ghost_frame + 4, inky_pos, 361);
		draw_sprite(spr_clyde, ghost_frame + 4, clyde_pos, 361);
	} else {
		if (blinky_eaten == 0) {
			draw_sprite(spr_power_ghost, ghost_frame, blinky_pos, 361);
		} else if ((animation_frame - blinky_eaten) < pause_frame_count) {
			draw_sprite(spr_bonus_points, 0, blinky_pos, 361);
		}

		if (pinky_eaten == 0) {
			draw_sprite(spr_power_ghost, ghost_frame, pinky_pos, 361);
		} else if ((animation_frame - pinky_eaten) < pause_frame_count) {
			draw_sprite(spr_bonus_points, 1, pinky_pos, 361);
		}

		if (inky_eaten == 0) {
			draw_sprite(spr_power_ghost, ghost_frame, inky_pos, 361);
		} else if ((animation_frame - inky_eaten) < pause_frame_count) {
			draw_sprite(spr_bonus_points, 2, inky_pos, 361);
		}

		if (clyde_eaten == 0) {
			draw_sprite(spr_power_ghost, ghost_frame, clyde_pos, 361);
		} else if ((animation_frame - clyde_eaten) < pause_frame_count) {
			draw_sprite(spr_bonus_points, 3, clyde_pos, 361);
		}
	}
}

