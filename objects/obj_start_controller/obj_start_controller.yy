{
    "id": "8452bb30-dcd0-4391-b93b-ab9b339eabff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_start_controller",
    "eventList": [
        {
            "id": "fc64783e-6b5f-4186-85bf-8188464abe18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8452bb30-dcd0-4391-b93b-ab9b339eabff"
        },
        {
            "id": "a0559b9d-2fe6-427f-9a6b-5e95706b9171",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8452bb30-dcd0-4391-b93b-ab9b339eabff"
        },
        {
            "id": "c29a6ff9-5234-46a6-ad3f-af589c96c348",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8452bb30-dcd0-4391-b93b-ab9b339eabff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6331fc9c-388b-4388-897b-99fe52867775",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}