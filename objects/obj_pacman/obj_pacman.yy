{
    "id": "c6297e9d-ad69-4b23-bcf0-b380d4896ac9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pacman",
    "eventList": [
        {
            "id": "6cf20e10-be99-4534-813d-9b7054a02ec2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c6297e9d-ad69-4b23-bcf0-b380d4896ac9"
        },
        {
            "id": "af09bd28-4e6c-4d6b-b751-dfa00a767b7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6297e9d-ad69-4b23-bcf0-b380d4896ac9"
        },
        {
            "id": "35e59e78-c2ee-4d40-863e-7eda152fdb20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "94f97512-b13e-4d4f-8335-2a8d8574bd9a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c6297e9d-ad69-4b23-bcf0-b380d4896ac9"
        },
        {
            "id": "2a4ad709-3be2-4fba-98b1-e1f980dae97e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3a283dc7-3dfe-4290-9d8c-e538402fd79e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c6297e9d-ad69-4b23-bcf0-b380d4896ac9"
        },
        {
            "id": "45f33f1d-9cea-4fd6-aa50-422531791880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c6297e9d-ad69-4b23-bcf0-b380d4896ac9"
        },
        {
            "id": "85633ce4-fece-433c-91be-4e1245262b7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f7a0971b-176d-4e49-9d1a-8b2885458f19",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c6297e9d-ad69-4b23-bcf0-b380d4896ac9"
        }
    ],
    "maskSpriteId": "455714a0-6989-4adf-8594-58afcab389d0",
    "overriddenProperties": null,
    "parentObjectId": "fc45faa0-1298-469d-bdb6-d809f69f732a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
    "visible": false
}