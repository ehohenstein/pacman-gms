if (keyboard_check_pressed(vk_left)) {
	want_direction = direction_left;
	key_pressed = true;
}
if (keyboard_check_pressed(vk_right)) {
	want_direction = direction_right;
	key_pressed = true;
}
if (keyboard_check_pressed(vk_up)) {
	want_direction = direction_up;
	key_pressed = true;
}
if (keyboard_check_pressed(vk_down)) {
	want_direction = direction_down;
	key_pressed = true;
}

if (global.game_state == global.game_state_init) {
	image_index = 1;
	animation_frame = 1;
} else if (global.game_state == global.game_state_normal) {
	if (key_pressed) {
		if (is_same_direction(to_intersection, from_intersection, want_direction)) {
			var intersection_temp = to_intersection;
			to_intersection = from_intersection;
			from_intersection = intersection_temp;
			if ((ghost_direction mod 2) == 0) {
				ghost_direction += 1;
			} else {
				ghost_direction -= 1;
			}
		}
		animation_timer = animation_frame_count;
		stopped = false;
		key_pressed = false;
	}

	event_inherited();

	if (step_towards()) {
		choose_pacman_goal();
	}

	if (eating > 0) {
		if (!audio_is_playing(snd_eating)) {
			audio_play_sound(snd_eating, 1, true);
		}
		eating--;
	} else {
		if (audio_is_playing(snd_eating)) {
			audio_stop_sound(snd_eating);
		}
	}
} else if (global.game_state == global.game_state_reset) {
	if (dying) {
		animation_timer--;
		if (animation_timer == 0) {
			animation_timer = dying_animation_frame_count;
			if (animation_frame == 12) {
				dying = false;
				visible = false;
				sprite_index = spr_pacman;
				var controller = instance_find(obj_game_controller, 0);
				controller.alarm[0] = room_speed * 0.5;
				lives--;
			} else {
				animation_frame++;
			}
		}
	}
	image_index = animation_frame;
} else if (global.game_state == global.game_state_paused) {
	eating_timer--;
	if (eating_timer == 0) {
		visible = true;
		global.game_state = global.game_state_normal;
	}
} else if (global.game_state == global.game_state_level_complete) {
	image_index = 1;
}