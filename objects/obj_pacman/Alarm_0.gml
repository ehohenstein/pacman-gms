dying = true;

for (var i = 0; i < instance_number(obj_ghost); i++) {
	var instance = instance_find(obj_ghost, i);
	instance.visible = false;
}

animation_timer = dying_animation_frame_count;
animation_frame = 0;

sprite_index = spr_pacman_dying;
image_index = 0;

audio_play_sound(snd_pacman_death, 1, false);
