score += 10;
if (score > global.high_score) {
	global.high_score = score;
}
if ((floor((score - 10) / 10000) < floor(score / 10000)) && (lives < 5)) {
	audio_play_sound(snd_extra_life, 1, false);
	lives++;
}

eating = room_speed * 0.5;

with (other) {
	instance_destroy();
}

if ((instance_number(obj_dot) + instance_number(obj_power_dot)) == 0) {
	global.game_state = global.game_state_level_complete;
}
