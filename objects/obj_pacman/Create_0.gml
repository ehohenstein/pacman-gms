event_inherited();

dying = false;
dying_animation_frame_count = 7;

animation_frames = 4;

in_jail = false;

ghost_direction = direction_left;

want_direction = direction_left;
key_pressed = false;

ghost_speed = 1.7;
speed_scale = 1.0;

x = 321;
y = 541;

from_intersection = 52;
to_intersection = 51;

eating = 0;

power_timer = 0;
eating_timer = 0;
ghosts_eaten = 0;
