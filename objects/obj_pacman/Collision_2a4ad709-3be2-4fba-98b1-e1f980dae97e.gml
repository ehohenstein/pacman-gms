if (global.game_state == global.game_state_normal) {
	if (other.power_timer > 0) {
		if (!other.eaten) {
			eating_timer = room_speed * 0.5;
			other.eaten = true;
			other.eaten_timer = room_speed * 0.5;
			other.eaten_index = ghosts_eaten;
			var score_increase = (200 * power(2, ghosts_eaten));
			ghosts_eaten++;
			score += score_increase;
			if (score > global.high_score) {
				global.high_score = score;
			}
			if ((floor((score - score_increase) / 10000) < floor(score / 10000)) && (lives < 5)) {
				audio_play_sound(snd_extra_life, 1, false);
				lives++;
			}
			visible = false;
			audio_stop_all();
			audio_play_sound(snd_ghost_death, 1, false);
			global.game_state = global.game_state_paused;
		}
	} else {
		global.game_state = global.game_state_reset;

		audio_stop_all();
	
		alarm[0] = room_speed * 0.5;
	}
}

