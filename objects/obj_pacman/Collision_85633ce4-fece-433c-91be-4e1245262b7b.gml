score += 50;
if (score > global.high_score) {
	global.high_score = score;
}
if ((floor((score - 50) / 10000) < floor(score / 10000)) && (lives < 5)) {
	audio_play_sound(snd_extra_life, 1, false);
	lives++;
}

for (var i = 0; i < instance_number(obj_ghost); i++) {
	var instance = instance_find(obj_ghost, i);
	instance.power_timer = (room_speed * 8 * power(0.9, global.level));
}

ghosts_eaten = 0;

with (other) {
	instance_destroy();
}

if ((instance_number(obj_dot) + instance_number(obj_power_dot)) == 0) {
	global.game_state = global.game_state_level_complete;
}
