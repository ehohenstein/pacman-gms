{
    "id": "7c0285ea-2a7a-4e2b-be28-02eae8ca4b05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inky",
    "eventList": [
        {
            "id": "9b222ac2-b19d-4ae0-bc0d-08178020ed6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c0285ea-2a7a-4e2b-be28-02eae8ca4b05"
        },
        {
            "id": "ba68cf45-2a2a-4174-b99d-57576402ee51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7c0285ea-2a7a-4e2b-be28-02eae8ca4b05"
        }
    ],
    "maskSpriteId": "0839a526-381b-49b2-9fe9-8692e5df78ce",
    "overriddenProperties": null,
    "parentObjectId": "3a283dc7-3dfe-4290-9d8c-e538402fd79e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
    "visible": true
}