global.option_destination_key = "destination";
global.option_available_to_jailed_ghost_key = "available_to_jailed_ghost";
global.option_available_to_free_ghost_key = "available_to_free_ghost";
global.option_available_to_eaten_ghost_key = "available_to_eaten_ghost";
global.option_weight_key = "weight";
global.option_tunnel_key = "is_tunnel";

global.intersection_options_key = "options";
global.intersection_location_x_key = "location_x";
global.intersection_location_y_key = "location_y";

global.game_state_init = 0;
global.game_state_normal = 1;
global.game_state_paused = 2;
global.game_state_reset = 3;
global.game_state_level_complete = 4;
global.game_state_over = 5;

global.game_state = global.game_state_normal;

global.high_score = 0

var game_map = array_create(71, noone);

game_map[0] = create_standard_intersection(75, 109, 1, true, 6, true);
game_map[1] = create_standard_intersection(173, 109, 0, false, 2, true, 7, true);
game_map[2] = create_standard_intersection(291, 109, 1, false, 9, true);
game_map[3] = create_standard_intersection(350, 109, 10, true, 4, false);
game_map[4] = create_standard_intersection(469, 109, 3, true, 12, true, 5, false);
game_map[5] = create_standard_intersection(567, 109, 4, true, 13, true);

game_map[6] = create_standard_intersection(75, 187, 0, false, 14, true, 7, true);
game_map[7] = create_standard_intersection(173, 187, 6, false, 1, false, 15, true, 8, true);
game_map[8] = create_standard_intersection(232, 187, 7, false, 16, true, 9, false);
game_map[9] = create_standard_intersection(291, 187, 8, true, 2, false, 10, false);
game_map[10] = create_standard_intersection(350, 187, 9, false, 3, false, 11, true);
game_map[11] = create_standard_intersection(409, 187, 10, false, 19, true, 12, false);
game_map[12] = create_standard_intersection(469, 187, 11, true, 4, false, 20, true, 13, false);
game_map[13] = create_standard_intersection(567, 187, 12, true, 5, false, 21, true);

game_map[14] = create_standard_intersection(75, 246, 6, false, 15, true);
game_map[15] = create_standard_intersection(173, 246, 14, false, 7, false, 33, true);
game_map[16] = create_standard_intersection(232, 246, 8, false, 17, true);
game_map[17] = create_standard_intersection(291, 246, 16, false, 23, true);
game_map[18] = create_standard_intersection(350, 246, 25, true, 19, false);
game_map[19] = create_standard_intersection(409, 246, 18, true, 11, false);
game_map[20] = create_standard_intersection(469, 246, 12, false, 36, true, 21, false);
game_map[21] = create_standard_intersection(567, 246, 20, true, 13, false);

game_map[22] = create_standard_intersection(232, 305, 34, false, 23, true);
game_map[23] = create_standard_intersection(291, 305, 22, false, 17, false, 24, true);

var intersection_24 = create_intersection(3, 321, 305);
add_intersection_option(intersection_24, 0, create_intersection_option(23, false, 1, false, true));
add_intersection_option(intersection_24, 1, create_intersection_option(28, true, 1, false, false));
add_intersection_option(intersection_24, 2, create_intersection_option(25, false, 1, false, true));
game_map[24] = intersection_24;

game_map[25] = create_standard_intersection(350, 305, 24, true, 18, false, 26, false);
game_map[26] = create_standard_intersection(409, 305, 25, true, 35, false);

var intersection_27 = create_intersection(2, 281, 355);
add_intersection_option(intersection_27, 0, create_intersection_option(30, false, 1, true, false));
add_intersection_option(intersection_27, 1, create_intersection_option(28, false, 1, false, true));
game_map[27] = intersection_27;

var intersection_28 = create_intersection(4, 321, 355);
add_intersection_option(intersection_28, 0, create_intersection_option(27, false, 1, false, false));
add_intersection_option(intersection_28, 1, create_intersection_option(31, false, 1, true, false));
add_intersection_option(intersection_28, 2, create_intersection_option(24, false, 1, false, true));
add_intersection_option(intersection_28, 3, create_intersection_option(29, false, 1, false, false));
game_map[28] = intersection_28;

var intersection_29 = create_intersection(2, 361, 355);
add_intersection_option(intersection_29, 0, create_intersection_option(28, false, 1, false, true));
add_intersection_option(intersection_29, 1, create_intersection_option(32, false, 1, true, false));
game_map[29] = intersection_29;

var intersection_30 = create_intersection(1, 281, 374);
add_intersection_option(intersection_30, 0, create_intersection_option(27, false, 1, true, true));
game_map[30] = intersection_30;

var intersection_31 = create_intersection(1, 321, 374);
add_intersection_option(intersection_31, 0, create_intersection_option(28, false, 1, true, true));
game_map[31] = intersection_31;

var intersection_32 = create_intersection(1, 361, 374);
add_intersection_option(intersection_32, 0, create_intersection_option(29, false, 1, true, true));
game_map[32] = intersection_32;

game_map[33] = create_standard_intersection(173, 364, 36, false, 40, false, 15, false, 34, true);
game_map[34] = create_standard_intersection(232, 364, 33, false, 37, false, 22, true);
game_map[35] = create_standard_intersection(409, 364, 38, false, 26, true, 36, false);
game_map[36] = create_standard_intersection(469, 364, 35, true, 20, false, 45, false, 33, false);

game_map[37] = create_standard_intersection(232, 423, 41, false, 34, true, 38, false);
game_map[38] = create_standard_intersection(409, 423, 37, false, 44, false, 35, true);

game_map[39] = create_standard_intersection(75, 482, 47, false, 40, true);
game_map[40] = create_standard_intersection(173, 482, 39, false, 49, false, 33, true, 41, true);
game_map[41] = create_standard_intersection(232, 482, 40, false, 37, true, 42, false);
game_map[42] = create_standard_intersection(291, 482, 41, true, 51, false);
game_map[43] = create_standard_intersection(350, 482, 52, false, 44, true);
game_map[44] = create_standard_intersection(409, 482, 43, false, 38, true, 45, false);
game_map[45] = create_standard_intersection(469, 482, 44, true, 54, false, 36, true, 46, false);
game_map[46] = create_standard_intersection(567, 482, 45, true, 56, false);

game_map[47] = create_standard_intersection(75, 541, 39, true, 48, false);
game_map[48] = create_standard_intersection(114, 541, 47, true, 58, false);
game_map[49] = create_standard_intersection(173, 541, 59, false, 40, true, 50, false);
game_map[50] = create_standard_intersection(232, 541, 49, true, 60, false, 51, true);
game_map[51] = create_standard_intersection(291, 541, 50, false, 42, true, 52, false);
game_map[52] = create_standard_intersection(350, 541, 51, false, 43, true, 53, false);
game_map[53] = create_standard_intersection(409, 541, 52, true, 63, false, 54, true);
game_map[54] = create_standard_intersection(469, 541, 53, true, 64, false, 45, true);
game_map[55] = create_standard_intersection(528, 541, 65, false, 56, true);
game_map[56] = create_standard_intersection(567, 541, 55, false, 46, true);

game_map[57] = create_standard_intersection(75, 600, 67, false, 58, true);
game_map[58] = create_standard_intersection(114, 600, 57, false, 48, false, 59, true);
game_map[59] = create_standard_intersection(173, 600, 58, false, 49, true);
game_map[60] = create_standard_intersection(232, 600, 50, true, 61, false);
game_map[61] = create_standard_intersection(291, 600, 60, true, 68, false);
game_map[62] = create_standard_intersection(350, 600, 69, false, 63, true);
game_map[63] = create_standard_intersection(409, 600, 62, false, 53, true);
game_map[64] = create_standard_intersection(469, 600, 54, true, 65, false);
game_map[65] = create_standard_intersection(528, 600, 64, true, 55, false, 66, false);
game_map[66] = create_standard_intersection(567, 600, 65, true, 70, false);

game_map[67] = create_standard_intersection(75, 659, 57, true, 68, true);
game_map[68] = create_standard_intersection(291, 659, 67, false, 61, true, 69, false);
game_map[69] = create_standard_intersection(350, 659, 68, false, 62, true, 70, false);
game_map[70] = create_standard_intersection(567, 659, 69, true, 66, true);

global.game_map = game_map;