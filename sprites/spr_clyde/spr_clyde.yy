{
    "id": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clyde",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d19c343-bdea-4184-a22d-85d37c7512cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "36d73328-8f7a-47d7-834b-65beb802b337",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d19c343-bdea-4184-a22d-85d37c7512cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d87516-d7d3-40f5-826d-cf11c11d7455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d19c343-bdea-4184-a22d-85d37c7512cd",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        },
        {
            "id": "28f57b39-fbac-4c97-a81b-cf2f096adf7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "a249f734-f751-4aeb-8a30-8ddcf45fe717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28f57b39-fbac-4c97-a81b-cf2f096adf7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "997468a0-32f1-4066-b63e-462587c43dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f57b39-fbac-4c97-a81b-cf2f096adf7c",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        },
        {
            "id": "4e6af98b-ecac-42bd-bebb-2ebf12abd1f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "8ffee2dd-798e-43d8-8dde-98671b997e7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6af98b-ecac-42bd-bebb-2ebf12abd1f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1006fc-b161-4460-8392-d80fa85e7b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6af98b-ecac-42bd-bebb-2ebf12abd1f6",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        },
        {
            "id": "72cb81d4-11a2-436b-8c8c-472e5b03d1cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "7f8d56ba-535c-49a0-990f-8dabf8656874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72cb81d4-11a2-436b-8c8c-472e5b03d1cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "047bf281-0064-49ac-82c0-b5d97f794ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72cb81d4-11a2-436b-8c8c-472e5b03d1cb",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        },
        {
            "id": "7695918d-9642-4e0f-b927-c833bf848b26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "0d4a167f-517b-4aa5-b0b6-fde20c134c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7695918d-9642-4e0f-b927-c833bf848b26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8083778a-e6d7-4668-b270-2fe8fdc1a4ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7695918d-9642-4e0f-b927-c833bf848b26",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        },
        {
            "id": "a365cb59-15cd-417d-a160-68a96840ef58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "b9d42531-2c6c-4689-9452-5cda2f55d255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a365cb59-15cd-417d-a160-68a96840ef58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "858b71a9-1840-487d-9695-69054fa2e203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a365cb59-15cd-417d-a160-68a96840ef58",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        },
        {
            "id": "9d571678-f599-49cb-bebf-3f9a2a84563e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "a322d71f-d086-4d5d-8b0b-6cd5759c04aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d571678-f599-49cb-bebf-3f9a2a84563e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34769b10-e599-45c4-b57f-e189fc81d5f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d571678-f599-49cb-bebf-3f9a2a84563e",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        },
        {
            "id": "7a0da4a6-a401-4822-a4ec-b1cfd264f467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "compositeImage": {
                "id": "09f9d412-e3e8-49f8-a4b8-4bcc7cd08ad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a0da4a6-a401-4822-a4ec-b1cfd264f467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5a311a2-af10-42cb-b64b-48d9379a0c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a0da4a6-a401-4822-a4ec-b1cfd264f467",
                    "LayerId": "775ba5bc-928f-419c-a9b2-13ad460fde2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "775ba5bc-928f-419c-a9b2-13ad460fde2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d5acd4d-7ecd-470a-ab95-e36bd0f0719a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 17
}