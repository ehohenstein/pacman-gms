{
    "id": "938191f4-fc99-4c44-aa20-0e6a4ec5c030",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_splash_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 0,
    "bbox_right": 600,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2137c1de-dbc6-484a-a535-e07e8ed04c0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "938191f4-fc99-4c44-aa20-0e6a4ec5c030",
            "compositeImage": {
                "id": "7811a721-a5b8-45dd-9f98-8354390c7b05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2137c1de-dbc6-484a-a535-e07e8ed04c0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "436aaea8-df42-4822-94a5-39e5ea7f8683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2137c1de-dbc6-484a-a535-e07e8ed04c0d",
                    "LayerId": "9a6fbac2-7a18-4fcf-9fbe-19c39477df85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 157,
    "layers": [
        {
            "id": "9a6fbac2-7a18-4fcf-9fbe-19c39477df85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "938191f4-fc99-4c44-aa20-0e6a4ec5c030",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 601,
    "xorig": 0,
    "yorig": 0
}