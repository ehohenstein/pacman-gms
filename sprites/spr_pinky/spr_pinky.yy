{
    "id": "d5463810-d1df-442d-87e8-9fea73e296e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pinky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cdc9022-2b96-4077-abcb-4b786cc87831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "a2a239d4-2cbf-4e77-8aaa-0b585b871f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cdc9022-2b96-4077-abcb-4b786cc87831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478442a9-8386-44f5-9552-0077ca5fdb29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cdc9022-2b96-4077-abcb-4b786cc87831",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        },
        {
            "id": "b669b455-a8a9-4c01-81ef-7fe2df1fcaa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "c79dd0a7-50c4-4911-8e33-cc4c934ccf38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b669b455-a8a9-4c01-81ef-7fe2df1fcaa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b7a6f8e-057d-4421-a3c7-9a514d7b4822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b669b455-a8a9-4c01-81ef-7fe2df1fcaa9",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        },
        {
            "id": "20f4b274-12b8-462f-9dc3-d7c2a387445f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "4c237dd9-47ee-47b8-a296-4ef66b51ac3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20f4b274-12b8-462f-9dc3-d7c2a387445f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66eaa327-7f17-4167-a294-9379efd1a489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20f4b274-12b8-462f-9dc3-d7c2a387445f",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        },
        {
            "id": "2fe9952d-d5a5-4681-bb0f-2dd7279d7833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "4878a37c-6538-43ac-900b-b3fc8642f773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe9952d-d5a5-4681-bb0f-2dd7279d7833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243726db-3b74-4dfe-b00b-d7403901d9fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe9952d-d5a5-4681-bb0f-2dd7279d7833",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        },
        {
            "id": "c87c0da3-90a3-4fd4-b1ea-926f07642b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "1c0d32ac-5701-40cd-bfb9-3547601ec970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c87c0da3-90a3-4fd4-b1ea-926f07642b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "433708cb-d763-42e2-b65c-3606a2658394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c87c0da3-90a3-4fd4-b1ea-926f07642b58",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        },
        {
            "id": "164f5a94-c815-4f2c-90fe-754acbcc78d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "82206512-7d99-4d4b-89e8-ad3efcddf767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164f5a94-c815-4f2c-90fe-754acbcc78d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c73b2d-f0df-448c-86f2-20ce81652dcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164f5a94-c815-4f2c-90fe-754acbcc78d9",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        },
        {
            "id": "8008a0e2-c561-4f7c-bf27-493a70108e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "8da966e1-e95e-4ec3-bf7e-2ea9685208eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8008a0e2-c561-4f7c-bf27-493a70108e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca66a4c-e535-434d-aeb5-723ec8323b80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8008a0e2-c561-4f7c-bf27-493a70108e62",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        },
        {
            "id": "48c4dc7a-b5c4-4961-9de4-1e1c84620a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "compositeImage": {
                "id": "72571f53-5f76-4678-a458-b2075f6d55f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48c4dc7a-b5c4-4961-9de4-1e1c84620a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "765cea55-af66-45d9-883d-0f2bf8bc1bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c4dc7a-b5c4-4961-9de4-1e1c84620a8e",
                    "LayerId": "2a02d304-f334-4a0c-8798-5b5c78db2b31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "2a02d304-f334-4a0c-8798-5b5c78db2b31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5463810-d1df-442d-87e8-9fea73e296e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 17
}