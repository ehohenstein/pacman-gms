{
    "id": "4bdce59d-e371-4787-a6b5-4da1665b6074",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pacman",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15a987a4-5c71-4a0a-8f77-2d6f82b941c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "3e18d39a-dcb0-4196-9fda-4e22761d0351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a987a4-5c71-4a0a-8f77-2d6f82b941c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91eea411-e3ed-48a0-bd59-f2fef8a645eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a987a4-5c71-4a0a-8f77-2d6f82b941c5",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "03839b72-670a-4dfb-9a3e-ce3de4b92a34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "4d52e75e-7fee-46d5-9dc3-f77c01e83482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03839b72-670a-4dfb-9a3e-ce3de4b92a34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05517c74-43a1-42a2-8e75-dbfcbead00a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03839b72-670a-4dfb-9a3e-ce3de4b92a34",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "0f5ba971-e7a1-46cd-8984-711cc7a1a4f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "fc408e81-7612-477f-aa8f-00a02b136b75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f5ba971-e7a1-46cd-8984-711cc7a1a4f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c825125-99a2-41ee-9e46-8bf5073cfb9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f5ba971-e7a1-46cd-8984-711cc7a1a4f3",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "ff3e7baf-6e1d-45d7-85cc-82105b4342c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "2c465f04-bc35-449d-9749-5f9f052850f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3e7baf-6e1d-45d7-85cc-82105b4342c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93e35f9c-e4e4-4b9e-b8e9-fed471840316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3e7baf-6e1d-45d7-85cc-82105b4342c7",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "bb727fd5-ff76-4adc-bac8-12e47c13a2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "f5172a3e-2d80-4328-9eaf-29795ae88b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb727fd5-ff76-4adc-bac8-12e47c13a2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f153980-e8f5-4244-bb8e-d434e7f3c1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb727fd5-ff76-4adc-bac8-12e47c13a2fc",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "59a207d5-e53e-48a1-a54a-2baf7b92cf94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "cdbd91f9-6dfa-4d9e-bbff-7200b95b5834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a207d5-e53e-48a1-a54a-2baf7b92cf94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09846d1a-5ee7-4cb7-95a5-5d32d8d335b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a207d5-e53e-48a1-a54a-2baf7b92cf94",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "917f05fe-f831-4d2d-bc5f-c85c62e76e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "d1b07c16-1786-466d-a24b-0ce45b241b73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "917f05fe-f831-4d2d-bc5f-c85c62e76e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e379e341-7be1-4fa3-9e70-85a2975cd9df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "917f05fe-f831-4d2d-bc5f-c85c62e76e68",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "29ea3ccf-e062-4897-92ac-48e464781538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "56a86c95-be5b-4011-8e46-9d59e6fff592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ea3ccf-e062-4897-92ac-48e464781538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "203fc254-ffb8-4a2d-9d79-ab79438400d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ea3ccf-e062-4897-92ac-48e464781538",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "d798a312-be92-4a89-8d9e-6aa9febe081b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "9cbba2d2-b7d6-4e39-a8ee-161a354a9f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d798a312-be92-4a89-8d9e-6aa9febe081b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9779128-ae4f-407c-b4c1-6185c46b1d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d798a312-be92-4a89-8d9e-6aa9febe081b",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "12f732e0-ed76-4367-b532-edadb8fe0062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "affa2b8c-5ddd-4f2c-b4f5-7475f94126d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f732e0-ed76-4367-b532-edadb8fe0062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfc81f05-71e8-41be-a628-f5be922ae26b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f732e0-ed76-4367-b532-edadb8fe0062",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "f25fb34e-5a43-4a4b-a144-e7a31512b4b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "13350248-70e8-4d9a-aac4-bb162b1acf31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25fb34e-5a43-4a4b-a144-e7a31512b4b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef09978-d6fb-41b6-ad33-9b387d327adb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25fb34e-5a43-4a4b-a144-e7a31512b4b8",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "5d3e7420-1006-4bee-aa08-6442ad9b718e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "aa5b5f69-63ec-4e6b-96d8-67f3b5bc5f25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d3e7420-1006-4bee-aa08-6442ad9b718e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece3f80b-81e4-4e25-83ae-53547ebf9859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d3e7420-1006-4bee-aa08-6442ad9b718e",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "ec385bdf-5665-4d8a-b0ec-59c47d8a10f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "b0f743b6-9d67-4134-8cdc-0d7f5dd095ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec385bdf-5665-4d8a-b0ec-59c47d8a10f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e385c16-623a-4488-85cb-8b214291efc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec385bdf-5665-4d8a-b0ec-59c47d8a10f2",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "e12bd719-ef61-4458-8edc-1244599c461f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "1678628c-9137-4fcc-9d41-44ec758c847c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12bd719-ef61-4458-8edc-1244599c461f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "534dd2b5-e6bf-49e4-af3c-9837368e455e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12bd719-ef61-4458-8edc-1244599c461f",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "7d4c9a17-1590-45ce-838e-879ce214f183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "d6aae6c8-cc22-4be9-80ae-069eb0e29291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d4c9a17-1590-45ce-838e-879ce214f183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e82967-4136-48dd-9b5e-2eda4a535521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d4c9a17-1590-45ce-838e-879ce214f183",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        },
        {
            "id": "51220e24-6241-4db2-b98b-a333d93e7c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "compositeImage": {
                "id": "5e35a7a2-2d73-4250-8e16-9deb879fe45b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51220e24-6241-4db2-b98b-a333d93e7c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df896b9-1f59-4454-819d-223a67f4b88c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51220e24-6241-4db2-b98b-a333d93e7c08",
                    "LayerId": "d84b362e-3234-453b-ae6f-e7f52ff5a870"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "d84b362e-3234-453b-ae6f-e7f52ff5a870",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bdce59d-e371-4787-a6b5-4da1665b6074",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 13
}