{
    "id": "455714a0-6989-4adf-8594-58afcab389d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_power_dot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4af91880-58dd-4160-8f83-526347084838",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455714a0-6989-4adf-8594-58afcab389d0",
            "compositeImage": {
                "id": "d5163a4b-3c17-454b-abb0-039cd412fb96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4af91880-58dd-4160-8f83-526347084838",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d72e8e-3a76-4142-b4d5-861b88abef73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4af91880-58dd-4160-8f83-526347084838",
                    "LayerId": "4e876754-1ccf-4bf4-aa31-75190dc64ba8"
                }
            ]
        },
        {
            "id": "1b06009d-447d-41e1-8e80-6a54444d3737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455714a0-6989-4adf-8594-58afcab389d0",
            "compositeImage": {
                "id": "8bc10ede-d4e5-4439-a496-28e4284477fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b06009d-447d-41e1-8e80-6a54444d3737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de31e2c5-e8a7-4189-886a-be105930701b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b06009d-447d-41e1-8e80-6a54444d3737",
                    "LayerId": "4e876754-1ccf-4bf4-aa31-75190dc64ba8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "4e876754-1ccf-4bf4-aa31-75190dc64ba8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "455714a0-6989-4adf-8594-58afcab389d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 6
}