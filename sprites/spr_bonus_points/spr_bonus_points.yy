{
    "id": "baa4ec60-6ef7-44ca-8d97-058bf229ecb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bonus_points",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 1,
    "bbox_right": 26,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0615809-6bd6-486f-9460-3a35a5f066cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa4ec60-6ef7-44ca-8d97-058bf229ecb7",
            "compositeImage": {
                "id": "502ddefa-8ace-4f41-bb1a-2dfa31b856c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0615809-6bd6-486f-9460-3a35a5f066cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d31ee7-460c-46f6-88e4-c4ef18155426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0615809-6bd6-486f-9460-3a35a5f066cd",
                    "LayerId": "3d3443a8-d9ad-4199-9eb5-7d7de1403df1"
                }
            ]
        },
        {
            "id": "ebff9a60-fa98-401e-a231-5c2b2da10907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa4ec60-6ef7-44ca-8d97-058bf229ecb7",
            "compositeImage": {
                "id": "6bb712c0-3b44-4756-95de-1f26e11491ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebff9a60-fa98-401e-a231-5c2b2da10907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f3ba1c-bedc-4017-a26b-c78d4ad21af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebff9a60-fa98-401e-a231-5c2b2da10907",
                    "LayerId": "3d3443a8-d9ad-4199-9eb5-7d7de1403df1"
                }
            ]
        },
        {
            "id": "e3f95176-373b-41d3-9c73-cfe3663b1596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa4ec60-6ef7-44ca-8d97-058bf229ecb7",
            "compositeImage": {
                "id": "f5b9ffb4-0f2f-4796-8d38-302dfde0860f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f95176-373b-41d3-9c73-cfe3663b1596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b70b3e4-8894-46d8-8199-2145529838cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f95176-373b-41d3-9c73-cfe3663b1596",
                    "LayerId": "3d3443a8-d9ad-4199-9eb5-7d7de1403df1"
                }
            ]
        },
        {
            "id": "e324a0a1-12d4-49b9-9151-cb8d4560ef28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa4ec60-6ef7-44ca-8d97-058bf229ecb7",
            "compositeImage": {
                "id": "818fa7f5-ac76-4da8-828d-558f944b6548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e324a0a1-12d4-49b9-9151-cb8d4560ef28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb9fc49c-4701-4412-b110-f7607a1adfbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e324a0a1-12d4-49b9-9151-cb8d4560ef28",
                    "LayerId": "3d3443a8-d9ad-4199-9eb5-7d7de1403df1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "3d3443a8-d9ad-4199-9eb5-7d7de1403df1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "baa4ec60-6ef7-44ca-8d97-058bf229ecb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}