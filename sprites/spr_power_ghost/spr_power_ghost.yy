{
    "id": "b431982f-0f6d-402c-b77e-65cfce956be0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_power_ghost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21864d43-51dc-4f64-a0b2-9a2c685ee875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "6bd2c4f7-ade7-4945-a525-1a2ff2b330b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21864d43-51dc-4f64-a0b2-9a2c685ee875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e92f35-b9f4-4b4a-b4de-37b9aba1c2a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21864d43-51dc-4f64-a0b2-9a2c685ee875",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        },
        {
            "id": "49b4d34e-90b2-4570-b9f1-f9bc13fe9d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "568a0325-932a-4f7d-adea-8672aff18dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b4d34e-90b2-4570-b9f1-f9bc13fe9d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1781db29-e951-47fa-b8c5-f983b47bfe06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b4d34e-90b2-4570-b9f1-f9bc13fe9d12",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        },
        {
            "id": "14fb21bb-3ae5-4aa4-a55d-a9e78555ab49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "06bb66f2-91d1-4267-8358-7b90185c3c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fb21bb-3ae5-4aa4-a55d-a9e78555ab49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "630f4239-ec61-4311-a3a0-1419a26e3b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fb21bb-3ae5-4aa4-a55d-a9e78555ab49",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        },
        {
            "id": "a8219c3a-ba60-480b-8af2-bc96520da136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "05e29279-4939-4928-833a-a0474f8a25ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8219c3a-ba60-480b-8af2-bc96520da136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec9e862-c181-4248-9b7a-358f9bf2c5e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8219c3a-ba60-480b-8af2-bc96520da136",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        },
        {
            "id": "568b6231-9a18-4b73-bc97-59447d94ac49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "c9ac5024-796a-4e75-be42-bd893f9f9821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "568b6231-9a18-4b73-bc97-59447d94ac49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f827a499-d67d-4c35-9579-6fc4302f02a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "568b6231-9a18-4b73-bc97-59447d94ac49",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        },
        {
            "id": "f536212a-ffd1-4662-a608-a82e0c7781a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "277b6677-78f0-4e81-a685-7e6f785af6ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f536212a-ffd1-4662-a608-a82e0c7781a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5fd9828-cfdd-413b-a304-4717ffa03e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f536212a-ffd1-4662-a608-a82e0c7781a4",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        },
        {
            "id": "daa784c6-3d32-422f-a154-4bd36651aadf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "f3c1c2cd-2b9f-44ca-a43b-9f9575c5856c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa784c6-3d32-422f-a154-4bd36651aadf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea800c9-38a4-47a4-b0b0-47dee0939be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa784c6-3d32-422f-a154-4bd36651aadf",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        },
        {
            "id": "714d04cd-5b9b-459b-a715-93cf2c6dce6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "compositeImage": {
                "id": "3313c2dc-fa44-4490-9268-dd5bc2dc610a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "714d04cd-5b9b-459b-a715-93cf2c6dce6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "641cf643-b35b-4b24-bc15-10e466d84af9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "714d04cd-5b9b-459b-a715-93cf2c6dce6e",
                    "LayerId": "60d9eabb-2fd4-4369-b5df-bae4fad380e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "60d9eabb-2fd4-4369-b5df-bae4fad380e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b431982f-0f6d-402c-b77e-65cfce956be0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 17
}