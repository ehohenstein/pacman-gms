{
    "id": "243991a0-4932-4ae2-84c5-d7fcfe0079cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_bg_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 737,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80a02f24-763d-4984-a566-9c7be3bda102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243991a0-4932-4ae2-84c5-d7fcfe0079cd",
            "compositeImage": {
                "id": "fbd38b45-079a-4ee0-8ca4-24cca8d6ea78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a02f24-763d-4984-a566-9c7be3bda102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4e0071d-e4ad-42ed-be11-2c5c9d4504f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a02f24-763d-4984-a566-9c7be3bda102",
                    "LayerId": "6ffc2052-1df0-4a53-a5f5-a87bd26257cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 738,
    "layers": [
        {
            "id": "6ffc2052-1df0-4a53-a5f5-a87bd26257cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "243991a0-4932-4ae2-84c5-d7fcfe0079cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}