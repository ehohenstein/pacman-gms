{
    "id": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pacman_dying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4c022e6-24bc-42ba-94fb-4ac839e66b11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "25074a80-5c97-4324-b51b-c86f80adf4a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4c022e6-24bc-42ba-94fb-4ac839e66b11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1830ba8c-9b0c-4bbf-884e-24700e4b3bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4c022e6-24bc-42ba-94fb-4ac839e66b11",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "127ff00e-1e84-4231-a2c3-f330eab7453a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "d6d4da57-ae7a-482b-86b6-52c48dbe5dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "127ff00e-1e84-4231-a2c3-f330eab7453a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51b45c21-6beb-4a26-9842-1c173f196716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "127ff00e-1e84-4231-a2c3-f330eab7453a",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "de38c3c8-f126-4ffb-82c7-27dde3bb2a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "e3b876e5-abb6-4756-babf-10066b4b0c81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de38c3c8-f126-4ffb-82c7-27dde3bb2a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a4498b-86d7-4f5a-acc9-9251d399a2f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de38c3c8-f126-4ffb-82c7-27dde3bb2a72",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "b11c45e0-2623-4cfe-9f20-7d72e69f04a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "ef24b2e4-e612-4643-90dc-64acd3a6a374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11c45e0-2623-4cfe-9f20-7d72e69f04a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "875f6cee-92bc-4fdc-a0f8-81ce13cbd7f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11c45e0-2623-4cfe-9f20-7d72e69f04a3",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "4d0b0a78-5b25-4f03-bec8-595e83e5b1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "f4eef423-d739-4968-a9d9-1048d2838639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d0b0a78-5b25-4f03-bec8-595e83e5b1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d54e3f1-c5fb-4ba8-9b48-7861ee7f61f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d0b0a78-5b25-4f03-bec8-595e83e5b1f5",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "d8af9b59-9924-4a40-8c5c-35036aa12d45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "37b4d09a-5a79-4fa1-9b93-446f3e10bcc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8af9b59-9924-4a40-8c5c-35036aa12d45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e29c62-6427-4311-a07b-febeb0472f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8af9b59-9924-4a40-8c5c-35036aa12d45",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "dc1e4ba8-e478-470a-af83-2621be4a316a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "d5b9ac42-92fb-4b8e-81bd-eb93a0cadc29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1e4ba8-e478-470a-af83-2621be4a316a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c94911f-1ed8-43a2-b6c4-5cd6cede23b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1e4ba8-e478-470a-af83-2621be4a316a",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "18330da6-b738-4394-8af5-9f08f7cd1c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "e9250954-4b20-4978-ada7-943381fcb63c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18330da6-b738-4394-8af5-9f08f7cd1c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e0911c5-55a5-4a30-9d66-95c8dbecba93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18330da6-b738-4394-8af5-9f08f7cd1c5b",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "896b6ec7-4296-4c7d-b92b-487a8e13ef94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "cb315ad3-4b08-47bf-afd1-aa78cd418626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896b6ec7-4296-4c7d-b92b-487a8e13ef94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c857c8-b404-4226-9691-b663596a9afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896b6ec7-4296-4c7d-b92b-487a8e13ef94",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "7a7d11b3-961b-42e3-a97a-fc06de4f20e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "73ececdd-c23f-4a8f-8b7e-84897babe905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7d11b3-961b-42e3-a97a-fc06de4f20e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237b300c-41ed-467f-a636-a1e4c4af7953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7d11b3-961b-42e3-a97a-fc06de4f20e6",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "b7982bac-2c47-4dd1-ad15-01c97b14de40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "7e21956e-b214-4015-b3f4-6b98149f73d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7982bac-2c47-4dd1-ad15-01c97b14de40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95cc9bfa-85b3-42da-80f5-6acb1560a960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7982bac-2c47-4dd1-ad15-01c97b14de40",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "de74feaa-3c60-4d2b-bb6e-8c09e52737e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "ca593837-0325-4a75-bf88-6860e348dab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de74feaa-3c60-4d2b-bb6e-8c09e52737e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d064438-c8b9-4c85-a80c-5d62da6bb7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de74feaa-3c60-4d2b-bb6e-8c09e52737e9",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        },
        {
            "id": "a5789b7d-e775-43a9-b3f2-792c9aa7c658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "compositeImage": {
                "id": "10e47810-cf1b-473d-b0df-70eb2f0e05e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5789b7d-e775-43a9-b3f2-792c9aa7c658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90e5178b-d8b9-475d-b6ae-5ec3383b5711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5789b7d-e775-43a9-b3f2-792c9aa7c658",
                    "LayerId": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "65886dcd-3a7f-457a-8259-a5a61a6bc4cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "640d7350-e2ec-43e0-8f3f-f17b686f8166",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}