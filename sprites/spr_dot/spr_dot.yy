{
    "id": "0839a526-381b-49b2-9fe9-8692e5df78ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24f20a8b-0264-4f8b-8ce7-bf6874ab178b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0839a526-381b-49b2-9fe9-8692e5df78ce",
            "compositeImage": {
                "id": "05d9b9a3-1022-49ed-8b72-26b47589205c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f20a8b-0264-4f8b-8ce7-bf6874ab178b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2bfc1e5-3e2c-409c-b4f2-99d8ea9cc2c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f20a8b-0264-4f8b-8ce7-bf6874ab178b",
                    "LayerId": "c46652fb-dfe3-46bf-9e7c-74c6ef929ece"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "c46652fb-dfe3-46bf-9e7c-74c6ef929ece",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0839a526-381b-49b2-9fe9-8692e5df78ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 5
}