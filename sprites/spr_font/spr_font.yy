{
    "id": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "346277dc-ca04-4d3c-8aa6-1e776c131298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "7718bed2-687d-49cb-bf5b-08d96df0a244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "346277dc-ca04-4d3c-8aa6-1e776c131298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49b13e34-09d1-4c90-8bdb-759d0794b4ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "346277dc-ca04-4d3c-8aa6-1e776c131298",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "3f1a1080-9956-4a52-b73f-40131f06231d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "9aa78fa4-ceb6-471c-92bb-629f8a22bbe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1a1080-9956-4a52-b73f-40131f06231d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc4ae228-5c42-43c7-bb88-82e792b1768e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1a1080-9956-4a52-b73f-40131f06231d",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "d2a80efb-89fe-485f-b75d-91ad45718aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "4f7d2658-c035-4ac1-8dff-6912c544f33a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a80efb-89fe-485f-b75d-91ad45718aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5f66050-42f3-4e8a-be70-e6f795b215bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a80efb-89fe-485f-b75d-91ad45718aab",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "22e38558-0ee3-4d3e-aa65-0ccd0a90ace4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "f95fbcb8-e640-442e-8b7d-b62149a0d6b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e38558-0ee3-4d3e-aa65-0ccd0a90ace4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a21eb3-71f9-4f61-97aa-4a847e9552aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e38558-0ee3-4d3e-aa65-0ccd0a90ace4",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "89246896-0127-4ac5-ba20-dca5c88fc8d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "200e3457-3128-4c12-bdae-a7dc5a712e12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89246896-0127-4ac5-ba20-dca5c88fc8d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db2ba5c7-4380-4d5f-acf4-889803fdcb3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89246896-0127-4ac5-ba20-dca5c88fc8d9",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "c0410bad-81af-463e-a881-7305e26663e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "4643413e-365c-4b9a-a41a-18d40efa1b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0410bad-81af-463e-a881-7305e26663e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ccf6626-844d-4e2d-9211-282bc96e3636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0410bad-81af-463e-a881-7305e26663e5",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "1bacd48a-cf54-4e23-a22d-bacde5115130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "cf32396e-5ec7-49ed-bb66-6edafe3ba20d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bacd48a-cf54-4e23-a22d-bacde5115130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "069e02d0-d6a4-400c-b3e1-0adec7d99129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bacd48a-cf54-4e23-a22d-bacde5115130",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "252049dd-35b7-4e18-9217-8f3684f9f0b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "b98ca9f3-f628-41f6-82fb-32af2ff7387d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252049dd-35b7-4e18-9217-8f3684f9f0b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab37a4fe-bff9-44a7-a24c-4c09e22f6e12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252049dd-35b7-4e18-9217-8f3684f9f0b9",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "f569064f-a7d0-4e7c-9e34-fa9e886200ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "b6dd8c5c-9d29-4f05-9e36-37624a213dc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f569064f-a7d0-4e7c-9e34-fa9e886200ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0afadc08-2e7e-4f7d-804e-5c77d4cbdd10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f569064f-a7d0-4e7c-9e34-fa9e886200ab",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "79b083ec-4f1d-4acf-92f6-b8699af61682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "2e178a08-dca3-4af1-b1cb-e85e2a35f538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79b083ec-4f1d-4acf-92f6-b8699af61682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c441a7e5-5dc8-4d09-8cba-7957294944e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79b083ec-4f1d-4acf-92f6-b8699af61682",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "d03702d3-8eb5-4d22-8ef0-e8211e9e0d08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "a979bdb5-0a82-4256-b765-eaf33d3ca07a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d03702d3-8eb5-4d22-8ef0-e8211e9e0d08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb978368-2f23-49e3-a2d4-0b1091710776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d03702d3-8eb5-4d22-8ef0-e8211e9e0d08",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "7fb1fc7d-43ce-4598-ae97-9d20bf3404c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "6f63935c-d29e-45bd-ad19-27ebf55f1107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb1fc7d-43ce-4598-ae97-9d20bf3404c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "022ac29e-0be4-4590-adaf-4aea71890493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb1fc7d-43ce-4598-ae97-9d20bf3404c3",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "59a0d877-3305-45b7-aa92-c59bdcd9db28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "ee4e6166-9c2a-4cba-9237-b1d4f40bec52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a0d877-3305-45b7-aa92-c59bdcd9db28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5aec388-8e6e-499e-859a-82022b7f290a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a0d877-3305-45b7-aa92-c59bdcd9db28",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "abb3df1c-2bf8-4797-ba27-0d37f774a53a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "d59bfa1e-ca5f-4668-a78a-5b207751f4c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb3df1c-2bf8-4797-ba27-0d37f774a53a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c3acd3-e775-46dd-8e16-a1e3face2185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb3df1c-2bf8-4797-ba27-0d37f774a53a",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "4c75153c-2f10-4769-9039-c67ca889c3a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "bb9d91bf-f7b7-4729-bd2f-e5fdccb92590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c75153c-2f10-4769-9039-c67ca889c3a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97b226d-5edc-4c95-b5c3-d70042d4951a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c75153c-2f10-4769-9039-c67ca889c3a0",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "d7fb0104-2034-4dba-ad22-ff2efe2e3aad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "21f28cc5-d69a-46cf-aeb7-026cd639e0f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7fb0104-2034-4dba-ad22-ff2efe2e3aad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19db1201-31a5-4775-b28c-d6c2108ac80f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7fb0104-2034-4dba-ad22-ff2efe2e3aad",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "6f4eb252-6387-404b-b2d7-00046528514d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "64606492-8bba-403c-844a-b057669e274f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f4eb252-6387-404b-b2d7-00046528514d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91cfb3bb-8572-40e7-b5cb-204d6f6e11bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f4eb252-6387-404b-b2d7-00046528514d",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "8b63be28-358b-4769-b20d-1fca3d4a6f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "c3e7dce1-08b8-45cb-9a17-17b371d90b97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b63be28-358b-4769-b20d-1fca3d4a6f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "499cf189-929c-47fe-ad0b-6b37acee61f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b63be28-358b-4769-b20d-1fca3d4a6f01",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "0380583e-b4ee-4adc-bfc1-cf59decfadca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "42a1b8d0-dc72-433d-a359-6676737aee9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0380583e-b4ee-4adc-bfc1-cf59decfadca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd99588-ca29-4aa2-b714-42edc701daca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0380583e-b4ee-4adc-bfc1-cf59decfadca",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "839ddd23-a855-4ce9-a398-0dc77e716ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "d42d24a3-e5bb-4075-a5d5-55dab7f72066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839ddd23-a855-4ce9-a398-0dc77e716ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5938cf96-7133-49dd-bc05-41ccfb3daa74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839ddd23-a855-4ce9-a398-0dc77e716ed1",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "508a47ee-6058-4b71-b684-275604c2525b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "31764440-35ce-49aa-8d5e-2033389911e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "508a47ee-6058-4b71-b684-275604c2525b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da92f983-7758-4569-9c5d-0432f69c5aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "508a47ee-6058-4b71-b684-275604c2525b",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "acdd15ad-c17a-47b5-8d56-d6a19d31305d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "958153f0-7aa2-421c-8abb-6e8ef51a60a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acdd15ad-c17a-47b5-8d56-d6a19d31305d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d2985c-c2bc-4b53-bb67-dc48bf24c943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acdd15ad-c17a-47b5-8d56-d6a19d31305d",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "adcaf9c7-e93f-4c92-9a6c-3ddcd3444a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "ef058f9d-865b-45fd-ba76-2d389b689f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcaf9c7-e93f-4c92-9a6c-3ddcd3444a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb36f01-2ced-4d63-ace0-f0d01429ab49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcaf9c7-e93f-4c92-9a6c-3ddcd3444a3f",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "a1066a2e-355f-4956-8551-9ddb36fb54d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "2cc21979-820f-43c6-badb-c3ba7c3b3c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1066a2e-355f-4956-8551-9ddb36fb54d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05294f38-98bb-4bf6-b4eb-d0fc8da30a2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1066a2e-355f-4956-8551-9ddb36fb54d8",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "1867db92-2a7c-41e3-b097-32bb4bcd58bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "cabacb98-6493-4e0f-b1cc-eada6b4c9fa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1867db92-2a7c-41e3-b097-32bb4bcd58bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10d4a678-ee85-42ff-8fe9-d3230bec265d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1867db92-2a7c-41e3-b097-32bb4bcd58bb",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "b827b63b-bdf0-40ac-bfea-c7b2c4fbf27b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "2d69ff26-9840-4168-9af5-ee634c478332",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b827b63b-bdf0-40ac-bfea-c7b2c4fbf27b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ab97f6-4e8e-426c-a861-f9803c08e5cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b827b63b-bdf0-40ac-bfea-c7b2c4fbf27b",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "50af3f9f-7b37-4fcc-b39f-a12fc88dc084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "8ad5556f-2937-4f5f-bd19-ec7adc385070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50af3f9f-7b37-4fcc-b39f-a12fc88dc084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053e9c35-d5bf-42c9-8383-1238795e79e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50af3f9f-7b37-4fcc-b39f-a12fc88dc084",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "cfb20339-def4-4ed6-beb0-fc46819d9919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "d26d771b-ada7-4ac4-b551-54c824ed37df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb20339-def4-4ed6-beb0-fc46819d9919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bd0e468-dd8f-4f71-87fd-d52aacf14464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb20339-def4-4ed6-beb0-fc46819d9919",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "9f4df44b-1bd8-405d-832a-652dad81faa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "920ccd5e-aadb-492d-8bac-e88863ef770a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4df44b-1bd8-405d-832a-652dad81faa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3dbc040-57c9-4f9b-a6eb-717b565fbaee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4df44b-1bd8-405d-832a-652dad81faa6",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "565dbcec-0652-4251-8e96-32dc77fc71d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "20407b46-9976-47c0-95f6-0c8babf80ab2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565dbcec-0652-4251-8e96-32dc77fc71d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72a8c4ec-f244-4c16-ace0-c72ad8a441f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565dbcec-0652-4251-8e96-32dc77fc71d1",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "7779b5db-2792-4a2c-a263-e713e77fd8ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "e53ecdc9-c626-4e2d-81d4-7578d4dd1831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7779b5db-2792-4a2c-a263-e713e77fd8ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8499ba4d-366d-43c2-a410-a1b6d3d94b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7779b5db-2792-4a2c-a263-e713e77fd8ed",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "e3a9cdf5-5117-4caa-a315-69ef6a1707cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "445037f4-b199-45f2-b649-277d00f72f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3a9cdf5-5117-4caa-a315-69ef6a1707cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "988b13b9-5053-4420-af02-b75af678be25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3a9cdf5-5117-4caa-a315-69ef6a1707cb",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "8ae63a41-3504-4fd6-9406-5ec12eb8876b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "23cef74d-2052-4759-8a7f-936474c06dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ae63a41-3504-4fd6-9406-5ec12eb8876b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b612088-4fbd-496f-b8e9-56f9da888005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ae63a41-3504-4fd6-9406-5ec12eb8876b",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "d4f25279-c27a-478a-967e-0f20c5d80682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "59ae4e59-a140-43a9-b4ff-f3c8a1f7175b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4f25279-c27a-478a-967e-0f20c5d80682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501b1fc0-2953-4b57-901b-a44c5baf8605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4f25279-c27a-478a-967e-0f20c5d80682",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "5604cc57-6ddb-48dd-872c-909639ac42bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "76872424-fc54-426f-8421-562168495622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5604cc57-6ddb-48dd-872c-909639ac42bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01fadb4e-3d9f-434a-9b5f-2e9a11fb555b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5604cc57-6ddb-48dd-872c-909639ac42bf",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "68f5fc66-ecfc-4ed8-b054-94b819df8e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "a9f6d31c-03d0-4556-a80e-1b43ee0c44ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68f5fc66-ecfc-4ed8-b054-94b819df8e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ee6683-8ed1-4226-ba76-c890f6b138de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68f5fc66-ecfc-4ed8-b054-94b819df8e3d",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "ac63773b-57bf-463f-ac77-912086bdbc4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "5c9d1b86-4183-4e39-85fb-df8a647a33e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac63773b-57bf-463f-ac77-912086bdbc4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e05cbd63-952e-43f5-9da5-70a553e50e07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac63773b-57bf-463f-ac77-912086bdbc4c",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "c784ac0f-c040-4c65-9d98-7b50d2756880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "08990d27-1eb2-43b1-9a9c-70443dbdd8ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c784ac0f-c040-4c65-9d98-7b50d2756880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df83ed05-0f94-4d6f-84d4-bee34a1b241d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c784ac0f-c040-4c65-9d98-7b50d2756880",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "7cc89b40-36d7-4fc8-a10c-c755ed2f7546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "d8264e06-9eb2-4c62-8fb3-5d7816d5e762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc89b40-36d7-4fc8-a10c-c755ed2f7546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61ce985-7a08-46f7-a07e-e3417c24e7a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc89b40-36d7-4fc8-a10c-c755ed2f7546",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "8c4c06dd-229b-40ce-8021-d75060e8d577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "ab20dbce-b12b-41e1-82b7-99c8e4236233",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4c06dd-229b-40ce-8021-d75060e8d577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33950558-3195-4fe4-8bee-7fc71a1f0aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4c06dd-229b-40ce-8021-d75060e8d577",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "8a2dfb18-d682-4a56-a32d-47a7a83164a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "599d2f34-ac66-4be2-85ef-27abe80f6932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a2dfb18-d682-4a56-a32d-47a7a83164a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849194c1-cb74-4896-b48b-ac6950aed53d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a2dfb18-d682-4a56-a32d-47a7a83164a5",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "1b4f901c-baa7-4fbf-8c63-62d6c23eade2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "ded1bac0-f239-434e-bf62-de074870fbf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b4f901c-baa7-4fbf-8c63-62d6c23eade2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ae68814-f39a-4c8d-8a7c-475de107f280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b4f901c-baa7-4fbf-8c63-62d6c23eade2",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "6bc765a4-c85c-40e5-bd99-dbf99c836571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "3319947a-33c1-4691-b9d6-310b437ec913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc765a4-c85c-40e5-bd99-dbf99c836571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6eed81-d212-41d3-902b-b891c160289f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc765a4-c85c-40e5-bd99-dbf99c836571",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "131973a5-15d9-40e8-a246-68989f7a0bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "ab13d3d7-58c8-4f97-9726-fe569af02fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "131973a5-15d9-40e8-a246-68989f7a0bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1847a09a-9381-4373-8c27-8956d955558b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "131973a5-15d9-40e8-a246-68989f7a0bb1",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "54819e48-0fb7-4fef-8414-3d551fb61966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "9a4cdb64-4cec-41c2-a4e3-b7750b9ac18d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54819e48-0fb7-4fef-8414-3d551fb61966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3479aa7-aa76-4d3a-8a9a-a13ecffd00ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54819e48-0fb7-4fef-8414-3d551fb61966",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "74d76751-e11b-4461-91f3-0d0724281c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "7148a7f3-add2-4ac7-926a-a153b26bde33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74d76751-e11b-4461-91f3-0d0724281c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe8119f-abd0-4157-b6db-98bcf1b364f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74d76751-e11b-4461-91f3-0d0724281c60",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        },
        {
            "id": "cf64640b-ede7-4ba8-acab-59eeced8f5a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "compositeImage": {
                "id": "ac8e6145-f445-4287-aa7b-644723b4e3aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf64640b-ede7-4ba8-acab-59eeced8f5a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c556e64-9164-459e-b7a0-35185080307a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf64640b-ede7-4ba8-acab-59eeced8f5a0",
                    "LayerId": "68eda346-8a43-44fd-ba54-acf106de3424"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "68eda346-8a43-44fd-ba54-acf106de3424",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c4cb66a-dbf4-479e-ae30-fe36a1d00756",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}