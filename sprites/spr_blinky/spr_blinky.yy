{
    "id": "3953bebd-0a83-4f30-88a8-ce73140c8716",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blinky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f12e97b1-d13c-460c-ae74-157d482fd029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "5e7f807b-bcb7-409b-be6d-d80231899d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12e97b1-d13c-460c-ae74-157d482fd029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3226882-4246-49c9-a6f0-fb8cbf3c8d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12e97b1-d13c-460c-ae74-157d482fd029",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        },
        {
            "id": "2699822a-92a2-4db4-9730-27dbee30da21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "a29365da-879f-4ebb-91bf-a0afa2917a25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2699822a-92a2-4db4-9730-27dbee30da21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b40227b-be44-49f8-9865-252cc0a541d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2699822a-92a2-4db4-9730-27dbee30da21",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        },
        {
            "id": "f3b91259-bbed-4025-88a2-b27e75e94073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "cb56da0e-fedf-4a79-9b22-d3f00fb7dbd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3b91259-bbed-4025-88a2-b27e75e94073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "059e7860-f032-4bd9-bfd1-ade9cda95594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3b91259-bbed-4025-88a2-b27e75e94073",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        },
        {
            "id": "c14cc89d-2d0d-4d5c-9038-aaafb39c29ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "4f927921-3f6c-47ce-8314-986e3ce92f2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c14cc89d-2d0d-4d5c-9038-aaafb39c29ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a4b642-207f-4437-a056-c466889b033b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c14cc89d-2d0d-4d5c-9038-aaafb39c29ac",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        },
        {
            "id": "83dd7c59-3b2a-4e39-ac4a-237b0263ac88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "a79ba2c8-a002-4ff7-ba05-0110a12fae6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83dd7c59-3b2a-4e39-ac4a-237b0263ac88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1634ddaa-0862-4e7c-9aa1-67c1889c9439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83dd7c59-3b2a-4e39-ac4a-237b0263ac88",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        },
        {
            "id": "4d7c2e20-992f-4c00-9ffb-86653b15c2c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "2f81052c-7306-4eb4-aada-b3d511ef0b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7c2e20-992f-4c00-9ffb-86653b15c2c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec0dfa1a-4231-4409-92b0-2afa89651d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7c2e20-992f-4c00-9ffb-86653b15c2c9",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        },
        {
            "id": "29b7859c-a4fe-438c-9dd7-7030f6ae49b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "ff190426-6f05-48b3-8b2a-11b15ce0a25d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29b7859c-a4fe-438c-9dd7-7030f6ae49b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cdc0072-77dc-4976-963d-495ac3805a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29b7859c-a4fe-438c-9dd7-7030f6ae49b5",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        },
        {
            "id": "21791a9e-f836-4996-aa2d-66afdd8cbe14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "compositeImage": {
                "id": "c1e7bc38-fe31-4ff1-b149-396f2defe444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21791a9e-f836-4996-aa2d-66afdd8cbe14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c504966-a7ab-4188-8ee8-700a415d8045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21791a9e-f836-4996-aa2d-66afdd8cbe14",
                    "LayerId": "6a405937-c824-4863-8a78-7220e0f7f553"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "6a405937-c824-4863-8a78-7220e0f7f553",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3953bebd-0a83-4f30-88a8-ce73140c8716",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 17
}