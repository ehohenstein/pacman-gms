{
    "id": "7b381c46-6999-4c2c-bd38-384260309384",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 737,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c0555de-b66d-41fa-b020-336009ade7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b381c46-6999-4c2c-bd38-384260309384",
            "compositeImage": {
                "id": "0566f98d-b1c9-4e22-ade2-1c253a631668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c0555de-b66d-41fa-b020-336009ade7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "060be2b3-ebcc-4924-af1e-a7844e2a1586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c0555de-b66d-41fa-b020-336009ade7c4",
                    "LayerId": "56570ab3-d962-478c-a0a4-02b0cc3d90ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 738,
    "layers": [
        {
            "id": "56570ab3-d962-478c-a0a4-02b0cc3d90ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b381c46-6999-4c2c-bd38-384260309384",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}