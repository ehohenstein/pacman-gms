{
    "id": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66d6b2a7-a461-4a13-a1c9-7ffbe51b4697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "fca8e821-0b2b-489f-a464-aad9ae2b25db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d6b2a7-a461-4a13-a1c9-7ffbe51b4697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b70428-0e06-4ae5-9141-4edfb2510abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d6b2a7-a461-4a13-a1c9-7ffbe51b4697",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        },
        {
            "id": "e38119f4-443c-4806-a0a1-7c041378d2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "f2ead4b5-37ed-48cf-aa3d-0f55521181e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e38119f4-443c-4806-a0a1-7c041378d2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e188b52-5582-4135-9a68-0aba5aa18212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e38119f4-443c-4806-a0a1-7c041378d2e3",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        },
        {
            "id": "df2ebcfe-c73b-4ce1-b788-6960f2a869df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "5cce8549-4b5b-403b-8080-65c239aa0c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df2ebcfe-c73b-4ce1-b788-6960f2a869df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2542a67d-8ce1-43cd-bb44-93db78b2e820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df2ebcfe-c73b-4ce1-b788-6960f2a869df",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        },
        {
            "id": "6967acf6-e011-4d31-ad1e-5e075f8c21df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "168f0d55-380a-4b78-b937-a9aa335a9626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6967acf6-e011-4d31-ad1e-5e075f8c21df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20548bad-3ae1-4672-90a6-3208be1ce118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6967acf6-e011-4d31-ad1e-5e075f8c21df",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        },
        {
            "id": "a5ceb060-b582-493b-a2cd-6ffa7a404eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "847a3a7e-38e9-4a7d-a4cb-5dce0c907822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ceb060-b582-493b-a2cd-6ffa7a404eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9096432-e6cd-47c1-91cb-62219318c961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ceb060-b582-493b-a2cd-6ffa7a404eba",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        },
        {
            "id": "0547bff8-6ba1-46f0-8250-fe576c09a601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "777150fe-1684-411f-8b7f-b34e53dab17b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0547bff8-6ba1-46f0-8250-fe576c09a601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6b8d11-32f1-4fcc-8be2-886e3f1b9c5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0547bff8-6ba1-46f0-8250-fe576c09a601",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        },
        {
            "id": "fd2e2626-3676-44e3-aeab-6ef3c3a8e455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "e4464291-d386-435d-9cdd-bda54ae8e670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2e2626-3676-44e3-aeab-6ef3c3a8e455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f29088b2-7803-4a6d-b15d-724ee69351fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2e2626-3676-44e3-aeab-6ef3c3a8e455",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        },
        {
            "id": "15ea45c5-4721-4194-b3ee-b913eaded113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "compositeImage": {
                "id": "71db8f9f-83be-4a15-bd95-9c3f802e5028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ea45c5-4721-4194-b3ee-b913eaded113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab362679-d8e1-4c39-9a7e-d7da6f8c40aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ea45c5-4721-4194-b3ee-b913eaded113",
                    "LayerId": "3b9dedb7-c098-4269-9d5b-102b1006804d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "3b9dedb7-c098-4269-9d5b-102b1006804d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1b578c7-9800-44ee-88e0-bc423ac2ce00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 17
}